package in.jsw.ihbapp.utils;

import org.json.JSONObject;
public class ErrorHandler {
    private static String getErrorDescription(Integer code){
        switch (code){
            case 0:
                return "Unknown or unsupported request";
            case 101:
                return "Database Error";
            case 102:
                return "User does not exist";
            case 103:
                return "Unable to generate OTP";
            case 104:
                return "OTP missing";
            case 105:
                return "OTP expired";
            case 106:
                return "OTP incorrect";
            case 107:
                return "User is disabled";
            case 108:
                return "Invalid user";
            case 109:
                return "You do not have access to this URL";
            case 110:
                return "Invalid request data";
            case 111:
                return "OTP expired";
            case 112:
                return "Invalid OTP";
            case 113:
                return "Invalid Mobile Number";
            case 114:
                return "Invalid password";
            case 115:
                return "Hashing error";
            case 116:
                return "Role does not exist";
            case 117:
                return "Invalid credentials";
            case 118:
                return "Module not found";
            case 119:
                return "Feature not found";
            case 120:
                return "Missing rights for feature";
            case 121:
                return "Role not found";
            case 122:
                return "A role with this name already exists";
            case 123:
                return "Invalid mobile number";
            case 124:
                return "Invalid email";
            case 125:
                return "No reporting roles";
            case 126:
                return "Missing cities";
            case 127:
                return "A user with this email or mobile already exists";
            case 128:
                return "Invalid city";
            case 129:
                return "A city with this name already exists";
            case 130:
                return "City not found";
            case 131:
                return "You are not allowed to configure roles feature";
            case 132:
                return "City based users can not have access to city feature";
            case 133:
                return "Lead not found";
            case 134:
                return "A city with this name already exists";
            case 135:
                return "You are not authorized to add this role";
            case 136:
                return "Global Admin should have PAN India access";
            case 137:
                return "Storage error";
            case 138:
                return "Error in sending email";
            case 139:
                return "Error in generating lead";
            case 140:
                return "Contentful Error";
            default:
                return "Unknown or unsupported request";
        }
    }

    public static JSONObject getError(Integer code){
        JSONObject errorResponse = new JSONObject();
        errorResponse.put("status", "Fail");
        JSONObject errorObject = new JSONObject();
        errorObject.put("code",code);
        errorObject.put("description",getErrorDescription(code));
        errorResponse.put("error",errorObject);
        return errorResponse;
    }
}
