package in.jsw.ihbapp.utils;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.Random;

public class BasicUtils {

    public static String randomStr(int length, Boolean isOTP) {
        Random generator = new Random();
        return generateString(generator,  isOTP ? "0123456789" : "abcdefghijklmnopqrstuvwxyz1234567890", length);
    }

    public static String generateString(Random rng, String characters, int length) {
        char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }
        return new String(text);
    }

    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public static boolean isValidMobileNumber(String mobileNumber) {
        boolean result = false;
        if(mobileNumber.length()==10 && Long.valueOf(mobileNumber) != null && Long.valueOf(mobileNumber)>=6000000000L && Long.valueOf(mobileNumber) <= 9999999999L)
            result = true;
        return result;
    }
}
