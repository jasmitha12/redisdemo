package in.jsw.ihbapp.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.jsw.ihbapp.dao.Customer;
import in.jsw.ihbapp.dao.Address;
import in.jsw.ihbapp.dao.FamilyDetails;
import in.jsw.ihbapp.dao.HomeRequirement;
import in.jsw.ihbapp.services.*;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/")
@Service
public class CustomerRestController {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private AuthService authService;

    @Autowired
    private CustomerService customerService;


    @PostMapping(value = "/otp", consumes = "application/json", produces = {"application/json"})
    public JsonNode otp(@RequestBody Map<String, String> data) {
        System.out.println(data);
        return mapper.convertValue(authService.getCustomerByMobile(new JSONObject(data)).toMap(), JsonNode.class);
    }

    @PostMapping(value = "/verifyOTP", consumes = "application/json", produces = {"application/json"})
    public JsonNode verifyOTP(@RequestBody Map<String, String> data) {
        System.out.println(data);
        return mapper.convertValue(authService.verifyOTP(new JSONObject(data)).toMap(), JsonNode.class);
    }

    @PostMapping(value = "/saveProfileInfo", consumes = "application/json", produces = {"application/json"})
    public JsonNode saveProfileInfo(@RequestBody Map<String, Object> data) {
        JSONObject tokenStatus = authService.validateToken(new JSONObject(data));
        if(tokenStatus.opt("uid") == null){
            return mapper.convertValue(tokenStatus.toMap(), JsonNode.class);
        }
        data.remove("token");
        Customer customer = new Customer(tokenStatus.optString("uid"), tokenStatus.optJSONObject("data"),tokenStatus.optInt("enabled"));
        System.out.println(String.valueOf(customerService.saveProfileInfo(new JSONObject(data), customer)));
        return mapper.convertValue(customerService.saveProfileInfo(new JSONObject(data), customer).toMap(), JsonNode.class);
    }

}
