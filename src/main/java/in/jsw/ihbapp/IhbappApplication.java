package in.jsw.ihbapp;

import com.fasterxml.jackson.databind.ObjectMapper;
import in.jsw.ihbapp.services.AuthService;
import org.hibernate.SessionFactory;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;
@SpringBootApplication
public class IhbappApplication {


	public static void main(String[] args) {
		SpringApplication.run(IhbappApplication.class, args);
	}
}
