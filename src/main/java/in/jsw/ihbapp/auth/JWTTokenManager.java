package in.jsw.ihbapp.auth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.json.JSONObject;

import java.io.File;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class JWTTokenManager {

    public static String generateToken(JSONObject data){
        try {
            RSAPublicKey publicKey = readPublicKey(new File("./src/main/keys/jwt.key.pub"));
            RSAPrivateKey privateKey = readPrivateKey(new File("./src/main/keys/jwt.key"));
            Algorithm algorithm = Algorithm.RSA256(publicKey, privateKey);
            return JWT.create().withClaim("userId",data.getString("userId")).withIssuer("JSW").sign(algorithm);
        } catch (Exception exception){
            exception.printStackTrace();
            return "Error";
        }
    }

    public static String verifyToken(String token){
        try {
            RSAPublicKey publicKey = readPublicKey(new File("./src/main/keys/jwt.key.pub"));
            RSAPrivateKey privateKey = readPrivateKey(new File("./src/main/keys/jwt.key"));
            Algorithm algorithm = Algorithm.RSA256(publicKey, privateKey);
            JWTVerifier verifier = JWT.require(algorithm).withIssuer("JSW").build();
            DecodedJWT jwt = verifier.verify(token);
            return jwt.getClaims().get("userId").asString();
        } catch (Exception exception){
            return "Error";
        }
    }


    private static RSAPublicKey readPublicKey(File file) throws Exception {
        String key = Files.readString(file.toPath(), Charset.defaultCharset());
        String publicKeyPEM = key.replace("-----BEGIN PUBLIC KEY-----", "").replaceAll(System.lineSeparator(), "").replace("-----END PUBLIC KEY-----", "");
        byte[] encoded = Base64.getDecoder().decode(publicKeyPEM);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encoded);
        return (RSAPublicKey) keyFactory.generatePublic(keySpec);
    }

    private static RSAPrivateKey readPrivateKey(File file) throws Exception {
        KeyFactory factory = KeyFactory.getInstance("RSA");
        try (FileReader keyReader = new FileReader(file);
             PemReader pemReader = new PemReader(keyReader)) {
            PemObject pemObject = pemReader.readPemObject();
            byte[] content = pemObject.getContent();
            PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(content);
            return (RSAPrivateKey) factory.generatePrivate(privKeySpec);
        }
    }
}
