package in.jsw.ihbapp.dao;

import org.json.JSONObject;

public class HomeRequirement {
    public String id;
    public String type;
    public JSONObject data;

    public HomeRequirement(String uid, String type, JSONObject data){
        this.id = uid;
        this.type = type;
        this.data = data;
    }
}
