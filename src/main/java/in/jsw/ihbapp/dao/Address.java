package in.jsw.ihbapp.dao;

import org.json.JSONObject;

public class Address {
    public String id;
    public String type;
    public JSONObject data;

    public Address(String uid, String type, JSONObject data){
        this.id = uid;
        this.type = type;
        this.data = data;
    }
}
