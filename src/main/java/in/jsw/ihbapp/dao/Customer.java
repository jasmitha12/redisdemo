package in.jsw.ihbapp.dao;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import java.io.Serializable;
import java.util.List;


public class Customer implements Serializable {


    public String uid;
    public JSONObject data;
    public Integer enabled;

    public Customer(String uid, JSONObject data, Integer enabled) {
        this.uid = uid;
        this.data = data;
        this.enabled = enabled;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Customer() {
    }
}
