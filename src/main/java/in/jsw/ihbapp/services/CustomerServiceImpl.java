package in.jsw.ihbapp.services;

import in.jsw.ihbapp.dao.Address;
import in.jsw.ihbapp.dao.Customer;
import in.jsw.ihbapp.dao.FamilyDetails;
import in.jsw.ihbapp.dao.HomeRequirement;
import in.jsw.ihbapp.utils.BasicUtils;
import in.jsw.ihbapp.utils.ErrorHandler;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Value("${secret.password}")
    private String secretPassword;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
//    @Cacheable(value="", key="")
    public JSONObject getCustomerDetails(String uid) {
        JSONObject response = new JSONObject();
        JSONObject responseObject = new JSONObject();
        if(uid == null){
            response = ErrorHandler.getError(110);
            return response;
        }
        jdbcTemplate.query("SELECT id, uid, type, data, modified_dt, created_dt, enabled FROM g.userdata WHERE enabled = 1 AND uid = ? AND type IN ('Floor Plan','3D Walkthrough','Outdoor View','Family Details','Home Requirement','Plot Details','Site Address','Residential Address') ORDER BY created_dt",new Object[]{ uid}, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                if(responseObject.opt(rs.getString("type")) == null){
                    responseObject.put(rs.getString("type"),new JSONArray());
                }
                JSONObject data = new JSONObject(rs.getString("data"));
                data.put("id", rs.getString("id"));
                responseObject.optJSONArray(rs.getString("type")).put(data);
            }
        });
        return responseObject;
    }

    @Override
    //    @Cacheable(value="", key="")
    public JSONObject saveProfileInfo(JSONObject data, Customer customer)
    {
        JSONObject response = new JSONObject();
        if(customer == null){
            response = ErrorHandler.getError(123);
            return response;
        }
        if(data.optString("name") == null || data.optString("mobile") == null || data.optJSONObject("residentialAddress") == null || data.optJSONObject("siteAddress") == null){
            response = ErrorHandler.getError(110);
            return response;
        }
        if(!BasicUtils.isValidMobileNumber(data.optString("mobile"))){
            response = ErrorHandler.getError(113);
            return response;
        }
        if(data.optString("email") != null && !BasicUtils.isValidEmailAddress(data.optString("email"))){
            response = ErrorHandler.getError(124);
            return response;
        }
        customer.data.put("name",data.optString("name"));
        customer.data.put("mobile",data.optString("mobile"));
        customer.data.put("email",data.optString("email"));
        jdbcTemplate.query("SELECT id, uid, type, data, modified_dt, created_dt, enabled FROM g.userdata WHERE enabled = 1 AND uid = ? AND type IN ('Floor Plan','3D Walkthrough','Outdoor View','Family Details','Home Requirement','Plot Details','Site Address','Residential Address') ORDER BY created_dt",new Object[]{customer.uid}, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
            }
        });
        String[] addressKeys = new String[]{"streetAddress","locality","state","city","pincode"};
        final Address[] siteAddresses = new Address[1];
        final Address[] residentialAddresses = new Address[1];
        jdbcTemplate.query(("SELECT id, uid, type, data, modified_dt, created_dt, enabled FROM g.userdata WHERE type = 'Site Address' AND enabled = 1 AND uid = ?"), new Object[]{ customer.uid}, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                siteAddresses[0] = new Address(rs.getString("id"),rs.getString("type"),new JSONObject(rs.getString("data")));
            }
        });
        if(siteAddresses[0] != null){
            Address siteAddress = siteAddresses[0];
            for(int i=0; i<addressKeys.length; i++){
                if(data.optJSONObject("siteAddress").optString(addressKeys[i])!=null){
                    siteAddress.data.put(addressKeys[i], data.optJSONObject("siteAddress").optString(addressKeys[i]));
                }
            }
            jdbcTemplate.query(("UPDATE g.userdata SET data = ? :: JSONB WHERE id = ? AND enabled = 1 RETURNING *"), new Object[]{siteAddress.data.toString(), siteAddress.id}, new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet rs) throws SQLException { }
            });
        }else{
            jdbcTemplate.query(("INSERT INTO g.userdata (uid, data, type) VALUES (?, ? :: JSONB, ?) RETURNING *"), new Object[]{customer.uid, data.optJSONObject("siteAddress").toString(), "Site Address"}, new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet rs) throws SQLException { }
            });
        }
        jdbcTemplate.query(("SELECT id, uid, type, data, modified_dt, created_dt, enabled FROM g.userdata WHERE type = 'Residential Address' AND enabled = 1 AND uid = ?"), new Object[]{ customer.uid}, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                residentialAddresses[0] = new Address(rs.getString("id"),rs.getString("type"),new JSONObject(rs.getString("data")));
            }
        });
        if(residentialAddresses[0] != null){
            Address residentialAddress = residentialAddresses[0];
            for(int i=0; i<addressKeys.length; i++){
                if(data.optJSONObject("residentialAddress").optString(addressKeys[i])!=null){
                    residentialAddress.data.put(addressKeys[i], data.optJSONObject("residentialAddress").optString(addressKeys[i]));
                }
            }
            jdbcTemplate.query(("UPDATE g.userdata SET data = ?:: JSONB WHERE id = ? AND enabled = 1 RETURNING *"), new Object[]{residentialAddress.data.toString(), residentialAddress.id}, new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet rs) throws SQLException { }
            });
        }else{
            jdbcTemplate.query(("INSERT INTO g.userdata (uid, data, type) VALUES (?, ?:: JSONB, ?) RETURNING * "), new Object[]{customer.uid, data.optJSONObject("residentialAddress").toString(), "Residential Address"}, new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet rs) throws SQLException { }
            });
        }
        response.put("status", "Success");
        return response;
    }

    @Override
    //    @Cacheable(value="", key="")
    public JSONObject updatePlotDetails(JSONObject data, Customer customer) {
        JSONObject response = new JSONObject();
        if(customer == null){
            response = ErrorHandler.getError(123);
            return response;
        }
        if(data.optString("address") == null || data.optString("area") == null || data.optString("floors") == null){
            response = ErrorHandler.getError(110);
            return response;
        }
        String[] plotKeys = new String[]{"builtUpArea","entranceWidth","shape","orientation","roadLocation","roadWidth","sharedCompoundWall","immediateContext"};
        final Address[] plots = new Address[1];
        jdbcTemplate.query(("SELECT id, uid, type, data, modified_dt, created_dt, enabled FROM g.userdata WHERE type = 'Plot Details' AND enabled = 1 AND uid = ?"), new Object[]{ customer.uid}, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                plots[0] = new Address(rs.getString("id"),rs.getString("type"),new JSONObject(rs.getString("data")));
            }
        });
        if(plots[0] != null){
            Address plot = plots[0];
            for(int i=0; i<plotKeys.length; i++){
                if(data.optString(plotKeys[i])!=null){
                    plot.data.put(plotKeys[i], data.optString(plotKeys[i]));
                }
            }
            jdbcTemplate.query(("UPDATE g.userdata SET data = ?::text WHERE id = ? AND enabled = 1 RETURNING *"), new Object[]{plot.data.toString(), plot.id}, new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet rs) throws SQLException {

                }
            });
        }else{
            jdbcTemplate.query(("INSERT INTO g.userdata (uid, data, type) VALUES (?, ?::text, ?) RETURNING *"), new Object[]{customer.uid, data.toString(),  "Plot Details"}, new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet rs) throws SQLException {

                }
            });
        }

        response.put("status", "Success");
        return response;
    }

    @Override
    //    @Cacheable(value="", key="")
    public JSONObject updateHomeRequirement(JSONObject data, Customer customer) {
        JSONObject response = new JSONObject();
        if(customer == null){
            response = ErrorHandler.getError(123);
            return response;
        }
        if(data.optString("rooms") == null || data.optString("bedrooms") == null){
            response = ErrorHandler.getError(110);
            return response;
        }
        String[] requirementKeys = new String[]{"masterBedrooms","commonWashrooms","attachedWashrooms","kitchen","livingRoom","diningSpace","studyRoom","poojaRoom","guestRoom","storeRoom","twoWheelerParking","fourWheelerParking","courtyard","garden","patio"};
        final HomeRequirement[] requirements = new HomeRequirement[1];
        jdbcTemplate.query(("SELECT id, uid, type, data, modified_dt, created_dt, enabled FROM g.userdata WHERE type = 'Home Requirement' AND enabled = 1 AND uid = ?"), new Object[]{ customer.uid}, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                requirements[0] = new HomeRequirement(rs.getString("id"),rs.getString("type"),new JSONObject(rs.getString("data")));
            }
        });
        if(requirements[0] != null){
            HomeRequirement requirement = requirements[0];
            for(int i=0; i<requirementKeys.length; i++){
                if(data.optString(requirementKeys[i])!=null){
                    requirement.data.put(requirementKeys[i], data.optString(requirementKeys[i]));
                }
            }
            jdbcTemplate.query(("UPDATE g.userdata SET data = ?::text WHERE id = ? AND enabled = 1 RETURNING *"), new Object[]{requirement.data.toString(), requirement.id}, new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet rs) throws SQLException {

                }
            });
        }else{
            jdbcTemplate.query(("INSERT INTO g.userdata (uid, data, type) VALUES (?, ?) RETURNING *"), new Object[]{customer.uid, data.toString(),  "Home Requirement"}, new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet rs) throws SQLException {

                }
            });
        }
        response.put("status", "Success");
        return response;
    }

    @Override
    //    @Cacheable(value="", key="")
    public JSONObject updateFamilyDetails(JSONObject data, Customer customer) {
        JSONObject response = new JSONObject();
        if(customer == null){
            response = ErrorHandler.getError(123);
            return response;
        }
        if(data.optString("familyMembers") == null){
            response = ErrorHandler.getError(110);
            return response;
        }
        String[] familyDeatilsKeys = new String[]{"seniorCitizens","seniorCitizenCouple","adults","kids","infants"};
        final FamilyDetails[] details = new FamilyDetails[1];
        jdbcTemplate.query(("SELECT id, uid, type, data, modified_dt, created_dt, enabled FROM g.userdata WHERE type = 'Family Details' AND enabled = 1 AND uid = ?"), new Object[]{ customer.uid}, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                details[0] = new FamilyDetails(rs.getString("id"),rs.getString("type"),new JSONObject(rs.getString("data")));
            }
        });
        if(details[0] != null){
            FamilyDetails familyDetails = details[0];
            for(int i=0; i<familyDeatilsKeys.length; i++){
                if(data.optString(familyDeatilsKeys[i])!=null){
                    familyDetails.data.put(familyDeatilsKeys[i], data.optString(familyDeatilsKeys[i]));
                }
            }
            jdbcTemplate.query(("UPDATE g.userdata SET data = ?::text WHERE id = ? AND enabled = 1 RETURNING *"), new Object[]{familyDetails.data.toString(),  familyDetails.id}, new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet rs) throws SQLException {

                }
            });
        }else{
            jdbcTemplate.query(("INSERT INTO g.userdata (uid, data, type) VALUES (?, ?::text, ?) RETURNING *"), new Object[]{customer.uid, data.toString(), "Family Details"}, new RowCallbackHandler() {
                @Override
                public void processRow(ResultSet rs) throws SQLException {

                }
            });
        }
        response.put("status", "Success");
        return response;
    }

}
