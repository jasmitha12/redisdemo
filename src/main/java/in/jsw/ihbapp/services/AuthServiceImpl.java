package in.jsw.ihbapp.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import in.jsw.ihbapp.auth.JWTTokenManager;
import in.jsw.ihbapp.dao.Customer;
import in.jsw.ihbapp.utils.BasicUtils;
import in.jsw.ihbapp.utils.ErrorHandler;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

@Service
public class AuthServiceImpl implements AuthService{

    @Value("${secret.password}")
    private String secretPassword;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ObjectMapper objectMapper;



    private Customer customer;
    private Boolean queryError = false;
    private Boolean incorrectOTP = false;


    /*check if customer exists and generate OTP*/
    @Override
    @Cacheable(cacheNames = "Customer", key = "#data.opt('mobile')")
    public JSONObject getCustomerByMobile(JSONObject data) {
        JSONObject response = new JSONObject();
        System.out.println(response);


        if(data.opt("mobile") == null){
            response = ErrorHandler.getError(110);
            return response;
        }
        jdbcTemplate.query("SELECT uid AS uid, (g.pgp_sym_decrypt(data, ?)::jsonb) as data, enabled AS enabled FROM g.users WHERE enabled =1 AND (g.pgp_sym_decrypt(data, ?)::jsonb)->>'mobile' = ? AND type='INT' LIMIT 1",new Object[]{secretPassword, secretPassword, data.opt("mobile")}, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                customer = new Customer(rs.getString("uid"),new JSONObject(rs.getString("data")),rs.getInt("enabled"));
            }
        });

        if(customer == null){
            response = ErrorHandler.getError(123);
            return response;
        }
        customer.data.put("otp", BasicUtils.randomStr(4,true));
        //TODO: call send OTP and delete below line when SMS services is working
        customer.data.put("otp", "1234");
        customer.data.put("otpDt", new SimpleDateFormat("dd MMM yyyy hh:mm:ss a", Locale.ENGLISH).format(new Date()));
        jdbcTemplate.query("UPDATE g.users SET data = g.pgp_sym_encrypt(?::text, ?) WHERE uid = ? RETURNING *", new Object[]{customer.data.toString(), secretPassword, customer.uid}, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                if(rs == null){
                    queryError = true;
                }
            }
        });
        response.put("status","Success");
        response.put("userId",customer.uid);
        response.put("next","verifyOTP");
        System.out.println(response);
        return response;
    }

    @Override
//    @Cacheable(value="customer", key="#data.userId")
    public JSONObject verifyOTP(JSONObject data) {
//        hashOperations.put("UID", customer.getData(), customer);
        JSONObject response = new JSONObject();
        if(data.opt("userId") == null || data.opt("otp") == null){
            response = ErrorHandler.getError(110);
            return response;
        }
        jdbcTemplate.query("SELECT uid AS uid, g.pgp_sym_decrypt(data, ?) as data, enabled AS enabled FROM g.users WHERE uid = ? AND enabled =1 AND type='EXT' LIMIT 1",new Object[]{secretPassword, data.opt("userId")}, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                customer = new Customer(rs.getString("uid"),new JSONObject(rs.getString("data")),rs.getInt("enabled"));
                incorrectOTP = !customer.data.opt("otp").equals(data.opt("otp"));
            }
        });

        if(customer == null){
            response = ErrorHandler.getError(123);
            return response;
        }
        if(incorrectOTP){
            incorrectOTP = false;
            response = ErrorHandler.getError(106);
            return response;
        }
        JSONObject jwtData = new JSONObject();
        jwtData.put("userId", customer.uid);
        response.put("token", JWTTokenManager.generateToken(jwtData));
        response.put("status", "Success");
        response.put("user", customer.data);
        System.out.println(response);
        return response;
    }

    @Override
//    @Cacheable(value="", key="")
    public JSONObject validateToken(JSONObject data) {
        JSONObject response = new JSONObject();
        if(data.opt("token") == null){
            response = ErrorHandler.getError(110);
            return response;
        }
        String userId = JWTTokenManager.verifyToken(data.getString("token"));
        if(userId.equals("Error")){
            response.put("status", "Logout");
            return response;
        }
        jdbcTemplate.query("SELECT uid AS uid, g.pgp_sym_decrypt(data, ?) as data, enabled AS enabled FROM g.users WHERE uid = ? AND type = 'EXT' LIMIT 1", new Object[]{secretPassword, userId}, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                customer = new Customer(rs.getString("uid"),new JSONObject(rs.getString("data")),rs.getInt("enabled"));
                incorrectOTP = !customer.data.opt("otp").equals(data.opt("otp"));
            }
        });
        if(customer == null){
            response = ErrorHandler.getError(123);
            return response;
        }
        response.put("data",customer.data);
        response.put("uid",customer.uid);
        response.put("enabled",customer.enabled);
        return response;
    }
}
