package in.jsw.ihbapp.services;

import in.jsw.ihbapp.dao.Customer;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
public interface AuthService  {

    public JSONObject getCustomerByMobile(JSONObject data);
    public JSONObject verifyOTP(JSONObject data);
    public JSONObject validateToken(JSONObject data);
}
