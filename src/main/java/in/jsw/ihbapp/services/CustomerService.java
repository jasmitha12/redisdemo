package in.jsw.ihbapp.services;

import in.jsw.ihbapp.dao.Customer;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public interface CustomerService {
    public JSONObject getCustomerDetails(String uid);
    public JSONObject saveProfileInfo(JSONObject data, Customer customer);
    public JSONObject updatePlotDetails(JSONObject data, Customer customer);
    public JSONObject updateHomeRequirement(JSONObject data, Customer customer);
    public JSONObject updateFamilyDetails(JSONObject data, Customer customer);


}
