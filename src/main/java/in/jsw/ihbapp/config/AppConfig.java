package in.jsw.ihbapp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.security.Security;

@Configuration
public class AppConfig {

    @PostConstruct
    public void init(){
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }
}